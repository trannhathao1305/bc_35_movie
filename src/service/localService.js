export const USER_LOCAL = "USER_LOCAL";
export const userLocalService = {
  // get: lấy lên
  get: () => {
    let userJson = localStorage.getItem(USER_LOCAL);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  // set: lưu xuống trước
  set: (userData) => {
    let userJson = JSON.stringify(userData);
    localStorage.setItem(USER_LOCAL, userJson);
  },
  // xóa data
  remove: () => {
    localStorage.removeItem(USER_LOCAL);
  },
};
